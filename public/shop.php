<?php require_once("../resources/config.php"); ?>

<?php include(TEMPLATE_FRONT . DS . "header.php") ?>

    <!-- Page Content -->
    <div class="container">

        <header class="jumbotron hero-spacer text-center">
            <h1>This Is Techno Culture</h1>
        </header>

        <div class="row">

            <?php include(TEMPLATE_FRONT . DS . "side_nav.php") ?>

            <div class="col-md-9">

                <div class="row carousel-holder">

                    <div class="row">

                        <?php get_products(); ?>


                    </div> <!-- Row ends here -->

                </div>

            </div>

        </div>
        <!-- /.container -->

<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
