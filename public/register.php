<?php require_once("../resources/config.php"); ?>

<?php include(TEMPLATE_FRONT . DS . "header.php") ?>

    <!-- Page Content -->
    <div class="container">

      <header>
            <h1 class="text-center">Register</h1>
            <h2 class="text-center bg-warning"><?php display_message(); ?></h2>
        <div class="col-sm-4 col-sm-offset-5">
            <form class="" action="" method="post" enctype="multipart/form-data">

                <?php register_user() ?>


                <div class="form-group"><label for="name">
                    Name<input type="text" name="name" class="form-control"></label>
                </div>

                <div class="form-group"><label for="surname">
                    Surname<input type="text" name="surname" class="form-control"></label>
                </div>

                <div class="form-group"><label for="username">
                    Username<input type="text" name="username" class="form-control"></label>
                </div>

                <div class="form-group"><label for="email">
                    Email<input type="email" name="email" class="form-control"></label>
                </div>

                <div class="form-group"><label for="password">
                    Password<input type="password" name="password" class="form-control"></label>
                </div>

                <div class="form-group">
                  <input type="submit" name="register" class="btn btn-primary" >
                  <a href="login.php" class="btn btn-secondary">Login</a>
                </div>

            </form>
        </div>


    </header>


        </div>

<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
