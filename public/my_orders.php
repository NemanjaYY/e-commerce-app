<?php require_once("../resources/config.php"); ?>

<?php include(TEMPLATE_FRONT . DS . "header.php") ?>

<?php

if(!isset($_SESSION['username'])) {
    redirect("./");
}

?>

<div class="col-md-12">
    <div class="row">
        <h1 class="page-header">
            My Orders

        </h1>

        <h4 class= "bg-success"></h4>
    </div>

    <div class="row">
        <table class="table table-hover">
            <thead>

            <tr>
                <th>Amount</th>
                <th>Product</th>
                <th>Transaction</th>
                <th>Currency</th>
                <th>Status</th>

            </tr>
            </thead>
            <tbody>
            <?php display_my_orders(); ?>

            </tbody>
        </table>
    </div>

<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
