<?php require_once("../resources/config.php"); ?>

<?php include(TEMPLATE_FRONT . DS . "header.php") ?>


    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <div class="col-md-12">

                <div class="row carousel-holder">

                    <div class="col-md-12">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

                            <?php include(TEMPLATE_FRONT . DS . "slider.php") ?>

                        </div>

                    </div>

                <div class="row">

                  <h5 class="text-center">step forward</h5>
                  <h1 class="text-center">THIS IS SXABLON UNIVERSUM</h1>

                </div> <!-- Row ends here -->

                <div class="text-justify">
                    <h5>
                        ШАБЛОН is a community that throws monthly events in Subotica. Created as a platform that nurtures art and techno music.
                    </h5>
                    <h5>
                        This is a landing page dedicated to the forward-looking movement that will redefine society taste borders in aesthetics,
                        by doing great things with people that have a fire of creation in their body.
                    </h5>
                </div>

            </div>

        </div>

    </div>
    <!-- /.container -->

<?php include(TEMPLATE_FRONT . DS . "footer.php") ?>
