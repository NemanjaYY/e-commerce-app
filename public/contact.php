<?php require_once("../resources/config.php"); ?>

<?php include(TEMPLATE_FRONT . DS . "header.php") ?>

         <!-- Contact Section -->

        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Contact Us</h2>
                    <h3 class="section-subheading">
                      <?php display_message(); ?>
                    </h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form name="sentMessage" id="contactForm" method="post">

                        <?php send_message(); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input name="subject" type="text"  class="form-control" placeholder="Your Subject *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="message" class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button name="submit" type="submit" class="btn btn-xl">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2766.740138671191!2d19.656589215558046!3d46.09615697911347!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x474366daa7a658b9%3A0x4696291c19958835!2sStudio11!5e0!3m2!1sen!2srs!4v1539549854705" width="1200" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <!-- OUR TEAM -->
        <section id="ourTeam">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-title text-center">
                            <h1>our team</h1>
                            <span class="st-border"></span>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="team-member">
                            <div class="member-image">
                                <img class="img-responsive" src="photos/nemanja.jpg" alt="">
                                <div class="member-social">
                                </div>
                            </div>
                            <div class="member-info">
                                <h4>Nemanja Stojanovic</h4>
                                <span>Art Director</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="team-member">
                            <div class="member-image">
                                <img class="img-responsive" src="photos/aleksandra.jpg" alt="">
                                <div class="member-social">
                                </div>
                            </div>
                            <div class="member-info">
                                <h4>Aleksandra Pavlovic</h4>
                                <span>Manager of operations</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="team-member">
                            <div class="member-image">
                                <img class="img-responsive" src="photos/mona.jpg" alt="">
                                <div class="member-social">
                                </div>
                            </div>
                            <div class="member-info">
                                <h4>Mona Lacko</h4>
                                <span>HR</span>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3 col-sm-6">
                        <div class="team-member">
                            <div class="member-image">
                                <img class="img-responsive" src="photos/kristina.jpg" alt="">
                                <div class="member-social">
                                </div>
                            </div>
                            <div class="member-info">
                                <h4>Kristina Vrgov</h4>
                                <span>Resident</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /OUR TEAM -->



    </div>






    <!-- /.container -->
<?php include(TEMPLATE_FRONT .  "/footer.php");?>
