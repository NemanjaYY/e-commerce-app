<div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <a class="navbar-brand" href="index.php">Sxablon</a>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li>
                <a href="shop.php">Shop</a>
            </li>

            <?php if(isset($_SESSION['item_quantity']) && $_SESSION['item_quantity'] > 0) { ?>
             <li>
                <a href="checkout.php">Checkout</a>
            </li>
            <?php } ?>

            <?php if(isset($_SESSION['username'])) { ?>
            <li>
                <a href="my_orders.php">My Orders</a>
            </li>
            <?php } ?>

            <li>
                <a href="contact.php">Contact</a>
            </li>

            <?php if(!isset($_SESSION['username'])) { ?>
                <li>
                    <a href="login.php">Login</a>
                </li>
            <?php } ?>
            <?php if(isset($_SESSION['username']) && $_SESSION['role_id'] == 2) { ?>
                <li>
                    <a href="admin">Admin</a>
                </li>
            <?php }?>
        </ul>
        <?php // include(dirname(__FILE__) . '/../shared/logout_button.php'); ?>
        <?php if(isset($_SESSION['username'])) { ?>
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $_SESSION['username'] ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">

                    <li class="divider"></li>
                    <li>
                        <a href="../public/admin/logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
        <?php } ?>
    </div>
    <!-- /.navbar-collapse -->
</div>
<!-- /.container -->
