<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="description" content="Sxablon E commerce page">
    <meta name="keywords" content="Ecommerce, label, Subotia, Sxablon, Techno">
    <meta name="author" content="Nemanja Stojanovic">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta property="og:title" content="HTML5 Page With Dropdown Navigation">
    <link rel="shortcut icon" type="image/png" href="favicon.png">

    <title>Shop Homepage - Start Bootstrap Template</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/styles.css" rel="stylesheet">
    <link href="css/shop-homepage.css" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <?php include(TEMPLATE_FRONT . DS . "top_nav.php") ?>
    </nav>
