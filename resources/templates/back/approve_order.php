<?php require_once("../../config.php");

if(!(isset($_SESSION['username']) && $_SESSION['role_id'] == 2)) {
    exit();
}

if(isset($_GET['id'])) {

$query = query("UPDATE orders SET order_status='Completed' WHERE order_id = " . escape_string($_GET['id']) . " ");
confirm($query);

set_message("Order Approved");
redirect("../../../public/admin/index.php?orders");

} else {

redirect("../../../public/admin/index.php?orders");

}

 ?>
