<?php

$upload_directory = "uploads"; //global function

// ********* Helper Functions ********* //

function last_id(){

    global $connection;
    mysqli_insert_id($connection);
    return mysqli_insert_id($connection);

}



function set_message($msg) {

    if(!empty($msg)) {

        $_SESSION['message'] = $msg;
    } else {

        $msg = "";

    }

}



function display_message() {

    if(isset($_SESSION['message'])){

        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }

}



function redirect($location) {

    header("Location: $location ");

}



function query($sql) {

    global $connection;

    return mysqli_query($connection, $sql);

}



function confirm($result) {

    global $connection;

    if(!$result) {

        die("QUERY FAILED" . mysqli_error($connection));

    }
}



function escape_string($string) {

    global $connection;

    return mysqli_real_escape_string($connection, $string);


}



function fetch_array($result) {

    return mysqli_fetch_array($result);
}


// ********* FRONT END FUNCTIONS ********* //

// ********* Get Products ********* //



function get_products()
{

    $query = query(" SELECT * FROM products");
    confirm($query);

    while ($row = fetch_array($query)) {

        $product_image = display_image($row['product_image']);

        $add_to_cart = '';
        if (isset($_SESSION['username'])) {
            $add_to_cart = <<<DELIMETER
<a class="btn btn-primary" target="_blank" href="../resources/cart.php?add={$row['product_id']}">Add to cart</a>
DELIMETER;
        }


        $product = <<<DELIMETER
        
<div class="col-sm-4 col-lg-4 col-md-4">
    <div class="thumbnail">
        <a href="item.php?id={$row['product_id']}"><img src="../resources/{$product_image}" alt=""></a>
        <div class="caption">
            <h4 class="pull-right">&#36;{$row['product_price']}</h4>
            <h4><a href="item.php?id={$row['product_id']}">{$row['product_title']}</a>
            </h4>
            <p>{$row['short_desc']}</p>
            {$add_to_cart}
        </div>
    </div>
</div>

DELIMETER;

        echo $product;

    }

}



function get_categories() {

    $query = query("SELECT * FROM categories");
    confirm($query);

    while($row = fetch_array($query)) {

        $categories_links = <<<DELIMETER

<a href='category.php?id={$row['cat_id']}' class='list-group-item'>{$row['cat_title']}</a>

DELIMETER;

        echo $categories_links;

    }

}



function get_products_in_cat_page() {

    $query = query(" SELECT * FROM products WHERE product_category_id = " . escape_string($_GET['id']) . " ");
    confirm($query);

    while($row = fetch_array($query)) {

        $product_image = display_image($row['product_image']);


        $add_to_cart_cat = '';
        if (isset($_SESSION['username'])) {
            $add_to_cart_cat = <<<DELIMETER
<a href="../resources/cart.php?add={$row['product_id']}" class="btn btn-primary">Buy Now!</a>
DELIMETER;
        }


        $product = <<<DELIMETER

<div class="col-md-3 col-sm-6 hero-feature">
    <div class="thumbnail">
        <img src="../resources/{$product_image}" alt="">
        <div class="caption">
            <h3>{$row['product_title']}</h3>
            <p>Techno culture</p>
            <p>
                {$add_to_cart_cat}
                <a href="item.php?id={$row['product_id']}" class="btn btn-default">More Info</a>
            </p>
        </div>
    </div>
</div>

DELIMETER;

        echo $product;

    }

}




function get_products_in_shop_page() {

    $query = query(" SELECT * FROM products");
    confirm($query);

    while($row = fetch_array($query)) {

        $product_image = display_image($row['product_image']);

        $product = <<<DELIMETER

<div class="col-md-3 col-sm-6 hero-feature">
    <div class="thumbnail">
        <img src="../resources/{$product_image}" alt="">
        <div class="caption">
            <h3>{$row['product_title']}</h3>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
            <p>
                <a href="../resources/cart.php?add={$row['product_id']}" class="btn btn-primary">Buy Now!</a> <a href="item.php?id={$row['product_id']}" class="btn btn-default">More Info</a>
            </p>
        </div>
    </div>
</div>

DELIMETER;

        echo $product;

    }

}



function login_user() {
    if(isset($_POST['submit'])) {
        $username = escape_string($_POST['username']);
        $password = md5(escape_string($_POST['password']));

        $query = query("SELECT * FROM users WHERE username = '{$username}' AND password = '{$password}' ");
        confirm($query);
        $row = mysqli_fetch_array($query);

        if(mysqli_num_rows($query) == 0) {
            set_message("Your Password or Username are wrong");
            redirect("login.php");
        } else {

            $_SESSION['username'] = $username;
            $_SESSION['role_id'] = $row['role_id'];
            $_SESSION['user_id'] = $row['user_id'];


            if($row['role_id'] == 2) {
                redirect("admin");
            } else {
                redirect("index.php");
            }
        }
    }
}


function send_message() {

    if(isset($_POST['submit'])) {

        $to         = "nemanja.neomodern@gmail.com";
        $from_name  = $_POST['name'];
        $email      = $_POST['email'];
        $subject    = $_POST['subject'];
        $message    = $_POST['message'];

        $headers = "From: {$from_name} {$email}";

        $result = mail($to, $subject, $message, $headers);
        if(!$result){
            set_message("Sorry we could not send your message");
            redirect("contact.php");
        } else {
            set_message("Your Message has been sent");
            redirect("contact.php");
        }

    }


}




// ********* ADMIN ORDERS ********* //

function display_orders() {

    $query = query("SELECT * FROM orders");
    confirm($query);

    while($row = fetch_array($query)) {

        $approve_html = '';
        if ($row['order_status'] == "Pending") {
            $approve_html = "<a class=\"btn btn-success\" href=\"../../resources/templates/back/approve_order.php?id={$row['order_id']}\"><span class=\"glyphicon glyphicon-ok\"></span></a>";
        }


        $product_title = get_product_title($row['order_id']);
        $orders = <<<DELIMETER

<tr>
  <td>{$row['order_id']}</td>
  <td>{$row['order_amount']}</td>
  <td>{$product_title}</td>
  <td>{$row['order_transaction']}</td>
  <td>{$row['order_currency']}</td>
  <td>{$row['order_status']}</td>
  <td><a class="btn btn-danger" href="../../resources/templates/back/delete_order.php?id={$row['order_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>
  <td>{$approve_html}</td>
</tr>

DELIMETER;

        echo $orders;

    }

}


function display_my_orders() {

    $query = query("SELECT * FROM orders WHERE user_id = '{$_SESSION['user_id']}'");
    confirm($query);

    while($row = fetch_array($query)) {




        $product_title = get_product_title($row['order_id']);
        $orders = <<<DELIMETER

<tr>
  <td>{$row['order_amount']}</td>
  <td>{$product_title}</td>
  <td>{$row['order_transaction']}</td>
  <td>{$row['order_currency']}</td>
  <td>{$row['order_status']}</td>
</tr>

DELIMETER;

        echo $orders;

    }

}



function get_product_title($order_id) {

    $query = query("SELECT product_title FROM reports WHERE order_id = '{$order_id}'");
    confirm($query);

    $row = fetch_array($query);
//    while($row = fetch_array($query)) {
        return $row['product_title'];
//    }
}



// ********* ADMIN PRODUCTS PAGE ********* //

function display_image($picture) {

    global $upload_directory;

    return $upload_directory . DS . $picture;

}



function get_products_in_admin() {

    $query = query(" SELECT * FROM products");
    confirm($query);

    while($row = fetch_array($query)) {

        $category = show_product_category_title($row['product_category_id']);

        $product_image = display_image($row['product_image']);

        $product = <<<DELIMETER

      <tr>
        <td>{$row['product_id']}</td>
        <td>{$row['product_title']}<br>
      <a href="index.php?edit_product&id={$row['product_id']}"><img width='200' src="../../resources/{$product_image}" alt=""></a>
        </td>
        <td>{$category}</td>
        <td>{$row['product_price']}</td>
        <td>{$row['product_quantity']}</td>
        <td><a class="btn btn-danger" href="../../resources/templates/back/delete_product.php?id={$row['product_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>

      </tr>

DELIMETER;

        echo $product;

    }
}


function show_product_category_title($product_category_id) {

    $category_query = query("SELECT * FROM categories WHERE cat_id = '{$product_category_id}' ");
    confirm($category_query);

    while($category_row = fetch_array($category_query)) {
        return $category_row['cat_title'];


    }

}





// ********* ADDIN PRODUCTS IN ADMIN ********* //

function add_product()
{
    if (isset($_POST['publish'])) {

        $product_title = escape_string($_POST['product_title']);
        $product_category_id = escape_string($_POST['product_category_id']);
        $product_price = escape_string($_POST['product_price']);
        $product_description = escape_string($_POST['product_description']);
        $short_desc = escape_string($_POST['short_desc']);
        $product_quantity = escape_string($_POST['product_quantity']);
        $product_image = escape_string($_FILES['file']['name']);
        $image_temp_location = escape_string($_FILES['file']['tmp_name']);

        move_uploaded_file($image_temp_location, UPLOAD_DIRECTORY . DS . $product_image);

        $query = query("INSERT INTO products(product_title, product_category_id, product_price, product_description, short_desc, product_quantity, product_image) VALUES('{$product_title}','{$product_category_id}','{$product_price}','{$product_description}','{$short_desc}','{$product_quantity}','{$product_image}')");
        $last_id = last_id();
        confirm($query);
        set_message("New product witd ID {$last_id} was added");
        redirect("index.php?products");


    }
}




function show_categories_add_product_page() {

    $query = query("SELECT * FROM categories");
    confirm($query);

    while($row = fetch_array($query)) {

        $categories_option = <<<DELIMETER

<option value="{$row['cat_id']}">{$row['cat_title']}</option>

DELIMETER;

        echo $categories_option;

    }

}


// ********* UPDATING PRODUCT ********* //

function update_product() {

    if(isset($_POST['update'])) {

        $product_title        = escape_string($_POST['product_title']);
        $product_category_id  = escape_string($_POST['product_category_id']);
        $product_price        = escape_string($_POST['product_price']);
        $product_description  = escape_string($_POST['product_description']);
        $short_desc           = escape_string($_POST['short_desc']);
        $product_quantity     = escape_string($_POST['product_quantity']);
        $product_image        = escape_string($_FILES['file']['name']);
        $image_temp_location  = escape_string($_FILES['file']['tmp_name']);

        if(empty($product_image)) {

            $get_pic = query("SELECT product_image FROM products WHERE product_id =" .escape_string($_GET['id']). " ");
            confirm($get_pic);

            while($pic = fetch_array($get_pic)) {
                $product_image = $pic['product_image'];

            }

        }




        move_uploaded_file($image_temp_location , UPLOAD_DIRECTORY . DS . $product_image);

        $query = "UPDATE products SET ";
        $query .= "product_title         = '{$product_title}'         , ";
        $query .= "product_category_id   = '{$product_category_id }'  , ";
        $query .= "product_price         = '{$product_price}'         , ";
        $query .= "product_description   = '{$product_description}'   , ";
        $query .= "short_desc            = '{$short_desc}'            , ";
        $query .= "product_quantity      = '{$product_quantity}'      , ";
        $query .= "product_image         = '{$product_image}'           ";
        $query .= "WHERE product_id=" . escape_string($_GET['id']);

        $send_update_query = query($query);
        confirm($send_update_query);
        set_message("Product has been updated");
        redirect("index.php?products");


    }

}


// ********* CATEGORIES IN ADMIN ********* //

function show_categories_in_admin() {

    $category_query = query("SELECT * FROM categories");
    confirm($category_query);

    while($row = fetch_array($category_query)) {

        $cat_id = $row['cat_id'];
        $cat_title = $row['cat_title'];

        $category = <<<DELIMETER

<tr>
    <td>{$cat_id}</td>
    <td>{$cat_title}</td>
    <td><a class="btn btn-danger" href="../../resources/templates/back/delete_category.php?id={$row['cat_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>

</tr>

DELIMETER;

        echo $category;

    }

}



function add_category() {

    if(isset($_POST['add_category'])){
        $cat_title = escape_string($_POST['cat_title']);

        if(empty($cat_title) || $cat_title == " "){

            echo "<p class='bg-danger'>This cannot be empty</p>";
        } else {


            $insert_cat = query("INSERT INTO categories(cat_title) VALUES('{$cat_title}')");
            confirm($insert_cat);

            set_message("Category was created");

        }
    }

}



// ********* ADMIN USERS ********* //

function display_users() {

    $category_query = query("SELECT * FROM users");
    confirm($category_query);

    while($row = fetch_array($category_query)) {

        $user_id   = $row['user_id'];
        $username  = $row['username'];
        $email     = $row['email'];
        $password  = $row['password'];

        $user = <<<DELIMETER

<tr>
    <td>{$user_id }</td>
    <td>{$username}</td>
    <td>{$email}</td>
    <td><a class="btn btn-danger" href="../../resources/templates/back/delete_user.php?id={$row['user_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>

</tr>

DELIMETER;

        echo $user;

    }

}

function add_users() {

    if(isset($_POST['add_user'])) {

        $username   = escape_string($_POST['username']);
        $email      = escape_string($_POST['email']);
        $password   = escape_string($_POST['password']);
// $user_photo = escape_string($_FILES['file']['name']);
// $photo_temp = escape_string($_FILES['file']['timp_name']);

// move_uploaded_file($photo_temp, UPLOAD_DIRECTORY . DS . $user_photo);

        $query = query("INSERT INTO users(username, email, password) VALUES('{$username}','{$email}','{$password}')");
        confirm($query);

        set_message("User created");
        redirect("index.php?users");

    }


}


function register_user() {

    if(isset($_POST['register'])) {

        $name       = escape_string($_POST['name']);
        $surname    = escape_string($_POST['surname']);
        $username   = escape_string($_POST['username']);
        $email      = escape_string($_POST['email']);
        $password   = md5(escape_string($_POST['password']));

        $query = query("INSERT INTO users(name, surname, username, email, password) VALUES('{$name}','{$surname}','{$username}','{$email}','{$password}')");
        confirm($query);

        set_message("User Registered");
        redirect("index.php?users");

    }


}

// ********* REPORTS IN ADMIN ********* //


function get_reports() {

    $query = query(" SELECT * FROM reports");
    confirm($query);

    while($row = fetch_array($query)) {

        $report = <<<DELIMETER

      <tr>
        <td>{$row['report_id']}</td>
        <td>{$row['product_id']}</td>
        <td>{$row['order_id']}</td>
        <td>{$row['product_price']}</td>
        <td>{$row['product_title']}<br>
        <td>{$row['product_quantity']}</td>
        <td><a class="btn btn-danger" href="../../resources/templates/back/delete_report.php?id={$row['report_id']}"><span class="glyphicon glyphicon-remove"></span></a></td>

      </tr>

DELIMETER;

        echo $report;

    }
}


// ********* ADD SLIDES FUNCTION ********* //

function add_slides() {

    if(isset($_POST['add_slide'])) {

        $slide_title          = escape_string($_POST['slide_title']);
        $slide_image          = escape_string($_FILES['file']['name']);
        $slide_image_location = escape_string($_FILES['file']['tmp_name']);

        if(empty($slide_title) || empty($slide_image)){

            echo "<p class='bg-danger'>This field cannot be empty</p>";

        } else {

            move_uploaded_file($slide_image_location, UPLOAD_DIRECTORY . DS . $slide_image);

            $query = query("INSERT INTO slides(slide_title, slide_image) VALUES('{$slide_title}','{$slide_image}')");
            confirm($query);
            set_message("Slide Added");
            redirect("index.php?slides");

        }

    }

}



function get_current_slide_in_admin() {
    $query = query("SELECT * FROM slides ORDER BY slide_id DESC LIMIT 1");
    confirm($query);

    while($row = fetch_array($query)) {

        $slide_image = display_image($row['slide_image']);
        $slide_active_admin = <<<DELIMETER

  <img class="img-responsive" src="../../resources/{$slide_image}">

DELIMETER;

        echo $slide_active_admin;
    }

}



function get_active_slide() {
    $query = query("SELECT * FROM slides ORDER BY slide_id DESC LIMIT 1");
    confirm($query);

    while($row = fetch_array($query)) {

        $slide_image = display_image($row['slide_image']);
        $slide_active = <<<DELIMETER

<div class="item active">
    <img class="slide-image" src="../resources/{$slide_image}">
</div>

DELIMETER;

        echo $slide_active;
    }

}



function get_slide_thumbnails() {

    $query = query("SELECT * FROM slides ORDER BY slide_id ASC");
    confirm($query);

    while($row = fetch_array($query)) {

        $slide_image = display_image($row['slide_image']);
        $slide_thumb_admin = <<<DELIMETER

<div class="col-xs-6 col-md-3">
    <img class="img-responsive slide_image" src="../../resources/{$slide_image}">
</div>

DELIMETER;

        echo $slide_thumb_admin;
    }

}



function get_slides() {
    $query = query("SELECT * FROM slides");
    confirm($query);

    while($row = fetch_array($query)) {
        $slide_image = display_image($row['slide_image']);

        $slides = <<<DELIMETER

  <div class="item">
      <img class="slide-image" src="../resources/{$slide_image}">
  </div>

DELIMETER;

        echo $slides;
    }

}






?>




