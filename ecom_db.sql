-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2018 at 12:32 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecom_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_title` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_title`) VALUES
(12, 'T-shirts'),
(13, 'Other'),
(14, 'Releases');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_amount` float NOT NULL,
  `order_transaction` varchar(255) NOT NULL,
  `order_status` varchar(255) NOT NULL,
  `order_currency` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `order_amount`, `order_transaction`, `order_status`, `order_currency`) VALUES
(53, 0, 2, '323245', 'Completed', 'USD'),
(54, 0, 2, '323245', 'Completed', 'USD'),
(55, 0, 2, '323245', 'Completed', 'USD'),
(56, 0, 2, '323245', 'Completed', 'USD'),
(57, 0, 200, 'e5f6ad', 'Completed', 'USD'),
(58, 0, 150, '841172', 'Pending', 'USD'),
(59, 0, 25, 'f90f2a', 'Pending', 'USD'),
(60, 0, 50, '47d1e9', 'Pending', 'USD'),
(61, 9, 40, '502e4a', 'Pending', 'USD'),
(62, 6, 20, 'dd8eb9', 'Completed', 'USD'),
(63, 10, 55, 'addfa9', 'Completed', 'USD'),
(64, 10, 55, 'addfa9', 'Completed', 'USD');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_category_id` int(11) NOT NULL,
  `product_price` float NOT NULL,
  `product_quantity` int(11) NOT NULL,
  `product_description` text NOT NULL,
  `short_desc` text NOT NULL,
  `product_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `product_title`, `product_category_id`, `product_price`, `product_quantity`, `product_description`, `short_desc`, `product_image`) VALUES
(11, 'We Love Our Audience ', 12, 25, 30, 'Official T-shirt of Sxablon (White/One Size)', 'Collection 2017/18', 'shirt_01.jpg'),
(12, 'Smiley Dancing Face', 12, 20, 30, 'Official T-shirt of Sxablon (Black/One Size)', 'Collection 2017/18', 'shirt_02.jpg'),
(13, 'Smiley Dancing Face', 12, 20, 30, 'Official T-shirt of Sxablon (White/One Size)', 'Collection 2017/18', 'shirt_03.jpg'),
(14, 'Sxablon Logo', 12, 25, 45, 'Official T-shirt of Sxablon (Black/One Size)', 'Collection 2016/17', 'shirt_04.jpg'),
(15, 'Sxablon Logo', 12, 15, 45, 'Official T-shirt of Sxablon (White/One Size)', 'Collection 2016/17', 'shirt_05.jpg'),
(16, ' Sxablon Dancing Socks', 13, 7, 60, 'Official Socks of Sxablon (White / One Size / 100% Cotton )', 'Collection 2017/18', 'socks_01.jpg'),
(17, ' Sxablon Smiley Socks', 13, 7, 60, 'Smiley Socks of Sxablon (White / One Size / 100% Cotton )', 'Collection 2017/18', 'socks_02.jpg'),
(18, 'Sxablon White Cap ', 13, 11, 100, 'Official cap of Sxablon (White / One Size / 100% Cotton )', 'Collection 2017/18', 'cap_01.jpg'),
(19, 'Sxablon Black Cap ', 13, 11, 100, 'Official cap of Sxablon (White / One Size / 100% Cotton )', 'Collection 2017/18', 'cap_02.jpg'),
(20, 'Dance Dance V/A', 14, 13, 100, 'Dance Dance V/A recorded in Subotica Studio11. ', 'Catalog number 01', 'vinyl_01.jpg'),
(21, 'Weird Dance', 14, 13, 100, 'Weird Dance recorded in Subotica Studio11. ', 'Catalog number 02', 'vinyl_02.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `report_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_price` float NOT NULL,
  `product_title` varchar(255) NOT NULL,
  `product_quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`report_id`, `product_id`, `order_id`, `product_price`, `product_title`, `product_quantity`) VALUES
(29, 12, 53, 20, 'Smiley Dancing Face', 5),
(30, 11, 54, 25, 'We Love Our Audience ', 1),
(31, 15, 55, 15, 'Sxablon Logo', 1),
(32, 13, 56, 20, 'Smiley Dancing Face', 1),
(33, 12, 57, 20, 'Smiley Dancing Face', 10),
(34, 11, 58, 25, 'We Love Our Audience ', 6),
(35, 11, 59, 25, 'We Love Our Audience ', 1),
(36, 11, 60, 25, 'We Love Our Audience ', 2),
(37, 12, 61, 20, 'Smiley Dancing Face', 2),
(38, 12, 62, 20, 'Smiley Dancing Face', 1),
(39, 13, 63, 20, 'Smiley Dancing Face', 2),
(40, 15, 64, 15, 'Sxablon Logo', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `slide_id` int(11) NOT NULL,
  `slide_title` varchar(255) NOT NULL,
  `slide_image` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`slide_id`, `slide_title`, `slide_image`) VALUES
(9, 'slider_01', 'slider_01.jpg'),
(10, 'slider_02', 'slider_02.jpg'),
(11, 'slider_03', 'slider_03.jpg'),
(12, 'slider_04', 'slider_04.jpg'),
(13, 'slider_05', 'slider_05.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `surname`, `username`, `email`, `password`, `role_id`) VALUES
(5, 'Admin', 'Adminic', 'Nemanja', 'nemanja.neomdeorn@gmail.com', '202cb962ac59075b964b07152d234b70', 2),
(6, 'User', 'Useric', 'user', 'user@test.com', '202cb962ac59075b964b07152d234b70', 1),
(9, 'User2', 'User2', 'user2', 'user2@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(10, 'user3', 'user3ic', 'user3', 'user3@gmail.com', '202cb962ac59075b964b07152d234b70', 1),
(12, 'Nemanja2', 'Stojanovic', 'Nemanja2', 'nemanja.neomdeorn@gmail.com', '202cb962ac59075b964b07152d234b70', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`slide_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `slide_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
